<div align="center">

<a href="https://macroscope.tech" rel="noopener" target="_blank"><img width="150" src="./public/logo192.png" alt="Material-UI logo"></a>

# The Macroscope

[![pipeline status](https://gitlab.com/StraightOuttaCrompton/macroscope-front-end/badges/master/pipeline.svg?style=flat-square)](https://gitlab.com/StraightOuttaCrompton/macroscope-front-end/commits/master)
[![coverage report](https://gitlab.com/StraightOuttaCrompton/macroscope-front-end/badges/master/coverage.svg?style=flat-square)](https://gitlab.com/StraightOuttaCrompton/macroscope-front-end/commits/master)

Analysis of langauge made simple.

</div>

## Bugs and Enhancement Requests

We welcome contributions and feedback on the site! Please create a [new issue](https://gitlab.com/StraightOuttaCrompton/macroscope-front-end/issues/new) in our [issue tracker](https://gitlab.com/StraightOuttaCrompton/macroscope-front-end/issues) and we'll take a look. Or you can can contact us by [email](mailto:macroscope-project@protonmail.com).

## Browser Support

Recent versions of Chrome and Firefox will be actively supported.

## Contributing

To contribute to the repository follow these steps:

1. [Fork](https://docs.gitlab.com/ee/user/project/repository/forking_workflow.html#creating-a-fork) the repository.
2. [Clone](https://docs.gitlab.com/ee/gitlab-basics/start-using-git.html#clone-a-repository) your fork to your local environment using [git](https://git-scm.com/).
3. [Build](#building) the project locally.
4. Make your changes to the code.
5. Commit your changes and push them to the forked repository.
6. You can then make a [merge request](https://docs.gitlab.com/ee/user/project/repository/forking_workflow.html#merging-upstream) which we will then review.

## Building

Install [NodeJS](https://nodejs.org/en/) - we recommend just installing [nvm](https://github.com/nvm-sh/nvm) which manages node versions.

Configure the app using the [.env](#environment-configuration) file.

Then open the `macroscope-front-end` directory in a terminal and run

```
npm install && npm start
```

## Environment Variables

### Possible variables

| Variable name          | Description                      | Default                     |
| ---------------------- | -------------------------------- | --------------------------- |
| REACT_APP_BASE_API_URL | The base url for the backend API | https://api.macroscope.tech |
| REACT_APP_API_VERSION  | API version                      | v1                          |
| REACT_APP_API_KEY      | Key to access the API            | -                           |

### .env

The project can use a [.env](https://github.com/motdotla/dotenv) file for configuration.

This file is not included in the repository and you will have to create it yourself. Open the `macroscope-front-end` directory in a terminal and run

```
touch .env
```

#### Example `.env` file

```
REACT_APP_BASE_API_URL=https://apiurl
REACT_APP_API_VERSION=v1
REACT_APP_API_KEY=apikey
REACT_APP_NODE_ENV=production
```

## Available Scripts

In the project directory, you can run:

### `npm start`

Runs the app in the development mode.<br>
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.<br>
You will also see any lint errors in the console.

### `npm test`

Launches the test runner in the interactive watch mode.<br>
See the section about [running tests](https://facebook.github.io/create-react-app/docs/running-tests) for more information.

### `npm coverage`

Generates a coverage report located at [./coverage/lcov-report/index.html](./coverage/lcov-report/index.html)

### `npm run build`

Builds the app for production to the `build` folder.<br>
It correctly bundles React in production mode and optimizes the build for the best performance.

The build is minified and the filenames include the hashes.<br>
Your app is ready to be deployed!

See the section about [deployment](https://facebook.github.io/create-react-app/docs/deployment) for more information.

### `npm run eject`

**Note: this is a one-way operation. Once you `eject`, you can’t go back!**

If you aren’t satisfied with the build tool and configuration choices, you can `eject` at any time. This command will remove the single build dependency from your project.

Instead, it will copy all the configuration files and the transitive dependencies (Webpack, Babel, ESLint, etc) right into your project so you have full control over them. All of the commands except `eject` will still work, but they will point to the copied scripts so you can tweak them. At this point you’re on your own.

You don’t have to ever use `eject`. The curated feature set is suitable for small and middle deployments, and you shouldn’t feel obligated to use this feature. However we understand that this tool wouldn’t be useful if you couldn’t customize it when you are ready for it.

## Alternative Packages

### Graphs

-   https://github.com/danvk/dygraphs

## Info

-   git svg sourced from [svg porn](https://svgporn.com/).
-   Old front end ip 51.89.148.58:88. Works with search terms "love" and "test".
