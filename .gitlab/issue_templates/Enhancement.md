Your enhancement may already be requested!
Please search on the [enhancement tracker](https://gitlab.com/StraightOuttaCrompton/macroscope-front-end/issues?label_name%5B%5D=enhancement) before creating one.

## Please Describe The Problem To Be Solved
<!-- Please present a concise description of the problem to be addressed by this feature request. -->
<!-- Please be clear what parts of the problem are considered to be in-scope and out-of-scope. -->

## Possible Solution
<!-- 
Not obligatory, but please provide a concise description of your preferred solution. Things to address include:
* Difference from current behaviour
* Details of the technical implementation
* Tradeoffs made in design decisions
* Caveats and considerations for the future

If there are multiple solutions, please present each one separately. Save comparisons for the very end.
-->

<!-- Do not edit below this line -->

/label ~enhancement
